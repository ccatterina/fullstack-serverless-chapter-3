# Full stack Serverless: Chapter 3

Working example of chapter 3 notes app of the book [Full stack serverless](https://www.oreilly.com/library/view/full-stack-serverless/9781492059882/) using Vue.js, Nuxt and Buefy instead of React and Ant Design.

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
