import { API } from 'aws-amplify'
import { createNote, deleteNote, updateNote } from '~/src/graphql/mutations'
import { listNotes } from '~/src/graphql/queries'
import { onCreateNote } from '~/src/graphql/subscriptions'

export const state = () => ({
  notes: []
})

export const mutations = {
  setNotes (state, notes) {
    state.notes = notes
  },
  add (state, note) {
    state.notes.push(note)
  },
  remove (state, id) {
    const idx = state.notes.findIndex(n => n.id === id)
    state.notes.splice(idx, 1)
  },
  replace (state, note) {
    const idx = state.notes.findIndex(n => n.id === note.id)
    state.notes.splice(idx, 1, note)
  }
}

export const actions = {
  async getNotes ({ commit }) {
    try {
      const notes = await API.graphql({
        query: listNotes
      })
      commit('setNotes', notes.data.listNotes.items)
    } catch (err) {
      console.log('error: ', err)
    }
  },
  async addNote ({ commit }, note) {
    try {
      const newNote = await API.graphql({
        query: createNote,
        variables: { input: note }
      })
      commit('add', newNote.data.createNote)
    } catch (err) {
      console.log('error: ', err)
    }
  },
  async removeNote ({ commit }, id) {
    try {
      await API.graphql({
        query: deleteNote,
        variables: { input: { id } }
      })
      commit('remove', id)
    } catch (err) {
      console.log('error: ', err)
    }
  },
  async editNote ({ commit }, note) {
    try {
      const editedNote = await API.graphql({
        query: updateNote,
        variables: { input: note }
      })
      commit('replace', editedNote.data.updateNote)
    } catch (err) {
      console.log('error: ', err)
    }
  },
  subscribeToCreate ({ commit }) {
    const subscription = API.graphql({
      query: onCreateNote
    })
      .subscribe({
        next: (noteData) => {
          const note = noteData.value.data.onCreateNote
          if (this.$config.clientId !== note.clientId) {
            commit('add', note)
          }
        }
      })
    return () => subscription.unsubscribe()
  }
}
